function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function searchArticle() {
  let url = "http://127.0.0.1:8000/";
  const res = await fetch(url);
  console.log(res);
  const text = await res.json();
  await sleep(2000);

  if (res.ok) {
    return text;
  } else {
    throw new Error(text);
  }
}
